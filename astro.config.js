import { defineConfig } from 'astro/config';

import Tailwind from "@astrojs/tailwind";
import netlify from "@astrojs/netlify";
import React from '@astrojs/react';

// https://astro.build/config
export default defineConfig({
  output: 'server',
  adapter: netlify(),

  // Server for SPAs and highly interactive websites
  // Hybrid for performance and SEO; optimal for content heavy sites
  integrations: [Tailwind(), React()],
});