/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			colors: {
				'cultured': '#F7F7F7',
				'pale-yellow': '#F9F871',
				'light-green': '#80D07A',
				'turquoise': '#009F83',
				'teal': '#006A76',
				'dark-blue': '#00374D',
				'charcoal': '#020818',
				'white': '#FFFBF7'
			}
		},
	},
	plugins: [
		function ({ addComponents }) {
			addComponents({
				'.flex-center': {
					'@apply flex justify-center items-center text-center': {}
				},
				'.flex-center-col': {
					'@apply flex-center flex-col': {},
				},
				'.flex-center-row': {
					'@apply flex-center flex-row': {},
				},
			});
		},
	],
}
